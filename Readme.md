ASTEROIDS 2019

TEAM MEMBERS:

Owen Wells 101532546
Particle Effects
Level design
Debugging
3D Modelling

James Bravender-Coyle 7628560
3D Modelling
Level design
Highscore serialization
Gameplay Logic Scripts(Damage, Lives, Spawning, SceneManagement, Celestial Bodies, Powers, Pickups etc.)
Debugging

Jade Sendak 102502278
Team Organisation and documentation
Menus
Sound Aquisition
 
Mitchell Thomas 9988408
Asteroid spawning system + spawning placement
Script Wrapping


Controls: 
XBOX 360 Controller for Windows (Keyboard for entering player name for highscores)
Keyboard & Mouse

TOOLS, SERVICES & ONLINE RESOURCES
Slack
Audacity
Sourcetree
Trello
bitbucket
Maya
Google drive
Facebook Messenger
Blender

Asteroid Model: "How to make a realistic asteroid" https://www.blenderguru.com/tutorials/how-to-make-a-realistic-asteroid
Code that inspired ship controller: https://answers.unity.com/questions/44226/asteroids-style-acceleration.html
Pixel Digivolve Font: dafont.com https://www.dafont.com/pixel-digivolve.font
Planet Textures: https://www.solarsystemscope.com/textures/download/2k_earth_daymap.jpg
	https://www.solarsystemscope.com/textures/download/2k_stars.jpg
	https://www.solarsystemscope.com/textures/download/2k_jupiter.jpg
	https://www.solarsystemscope.com/textures/download/2k_saturn_ring_alpha.png
	https://www.solarsystemscope.com/textures/download/2k_makemake_fictional.jpg
	
Code for mesh destruction for planet exploding: https://answers.unity.com/questions/1006318/script-to-break-mesh-into-smaller-pieces.html

Background music 
Carpenter, J (2016). Last Sunrise, from Lost Themes II





