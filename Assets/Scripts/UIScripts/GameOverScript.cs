﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GameOverScript : MonoBehaviour
{
    public TextMeshProUGUI score;
    public float scoreFloat;
    public float scoreVisual;
    public float lerp;
    public float scoreLerpDuration;

    public bool newHighScore = false;
    public GameObject highScoreUIObject;
    public GameObject continueButtonUIObject;
    private Animator anim;
    public InputField enterName;
    

    private IEnumerator coroutine;

    // Start is called before the first frame update
    void Start()
    {
        anim = gameObject.GetComponent<Animator>();
        coroutine = DisplayScore(1.0f);
        StartCoroutine(coroutine);
        score.text = scoreFloat.ToString("F0");
        scoreVisual = GameManager.instance.score;

        HighScoreCheck();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void HighScoreCheck()
    {
        //just add it to the local highscore for now
        //probably bubble sort it into the list here to determine whether its a high score
        if (scoreVisual > GameManager.instance.localHighScoreList.storedScores[6])
        {
            GameManager.instance.localHighScoreList.storedScores[6] = scoreVisual;
            newHighScore = true;
        }

        continueButtonUIObject.SetActive(true);

    }



    private IEnumerator DisplayScore(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        if (score != null)
        { 
                lerp += Time.deltaTime / scoreLerpDuration;
                scoreFloat = (float)Mathf.Lerp(scoreFloat, scoreVisual, lerp);
                score.text = scoreFloat.ToString("F0");

           


            //play moving to the side text animation so you can enter your name & activate name
            if (newHighScore)
            {
                highScoreUIObject.SetActive(true);
                anim.Play("ScoreBounce");
                enterName.gameObject.SetActive(true);
                var se = new InputField.SubmitEvent();
                se.AddListener(delegate {StopInputAndEnterName(enterName, enterName.text);});
                enterName.onEndEdit = se;
            }


        }
        }

    void StopInputAndEnterName(InputField input, string arg0)
    {
        if (input.text.Length > 0) input.interactable = false;
        Debug.Log(arg0);
        GameManager.instance.localHighScoreList.playerName[6] = arg0;

        float t;
        string a;

        for (int p = 0; p <= GameManager.instance.localHighScoreList.storedScores.Length - 2; p++)
        {
            for (int i = 0; i <= GameManager.instance.localHighScoreList.storedScores.Length - 2; i++)
            {
                if (GameManager.instance.localHighScoreList.storedScores[i] < GameManager.instance.localHighScoreList.storedScores[i + 1])
                {
                    t = GameManager.instance.localHighScoreList.storedScores[i + 1];
                    a = GameManager.instance.localHighScoreList.playerName[i + 1];
                    GameManager.instance.localHighScoreList.storedScores[i + 1] = GameManager.instance.localHighScoreList.storedScores[i];
                    GameManager.instance.localHighScoreList.playerName[i + 1] = GameManager.instance.localHighScoreList.playerName[i];
                    GameManager.instance.localHighScoreList.storedScores[i] = t;
                    GameManager.instance.localHighScoreList.playerName[i] = a;
                }
            }
        }
        continueButtonUIObject.SetActive(true);
        GameManager.instance.SaveData();

    }

}
