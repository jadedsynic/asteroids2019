﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class MenuUIScript : MonoBehaviour
{
    [Header("UI Elements")]
    public GameObject mainMenuPanel;
    public GameObject highScoresUI;
    public GameObject optionsUI;
    EventSystem eventSystem;

    [Header("Buttons")]
    public Button newGameButton;
    public int newGameSceneIndex;
    public Button highScoreButton;
    public int highScoreSceneIndex;
    public Button optionsButton;
    public Button quitGameButton;
    public Button highScoresBackButton;
    public Button optionsBackButton;

    [Header("Sliders")]
    public Slider brightnessSlider;
    public Slider volumeSlider;
    
    
    // Start is called before the first frame update
    void Start()
    {
        newGameButton.onClick.AddListener(NewGame);
        highScoreButton.onClick.AddListener(HighScores);
        optionsButton.onClick.AddListener(Options);
        quitGameButton.onClick.AddListener(QuitGame);
        highScoresBackButton.onClick.AddListener(backtoMainMenufromHighScores);
        optionsBackButton.onClick.AddListener(backtoMainMenufromOptions);
        eventSystem = EventSystem.current;
    }

    void NewGame()
    {
        if(GameManager.instance.Lives != 3)
        {
            GameManager.instance.Lives = 3;
        }

        if(GameManager.instance.isPlayerDead)
        {
            GameManager.instance.isPlayerDead = false;
        }

        GameManager.instance.score = 0f;

        SceneManager.LoadScene(newGameSceneIndex);
    }

    void QuitGame()
    {
        Application.Quit();
    }

    void HighScores()
    {
        SceneManager.LoadScene(highScoreSceneIndex);
    } 

    void Options()
    {
        if (mainMenuPanel.activeSelf)
        {
            mainMenuPanel.gameObject.SetActive(false);
        }

        if (highScoresUI.activeSelf)
        {
            highScoresUI.gameObject.SetActive(false);
        }

        optionsUI.gameObject.SetActive(true);
        eventSystem.SetSelectedGameObject(optionsBackButton.gameObject);

    }

    void backtoMainMenufromHighScores()
    {
        highScoresUI.gameObject.SetActive(false);
        mainMenuPanel.gameObject.SetActive(true);
    }

    void backtoMainMenufromOptions()
    {
        optionsUI.gameObject.SetActive(false);
        mainMenuPanel.gameObject.SetActive(true);
        eventSystem.SetSelectedGameObject(newGameButton.gameObject);
    }

    private void Update()
    {
        brightnessSlider.value = GameManager.instance.GammaCorrection;
    }
}
