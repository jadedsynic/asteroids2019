﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class LevelMaster : MonoBehaviour
{
    public int gameOverSceneIndex;

    [Header("Level Settings")]
    public int partsNeeded;
    public GameObject spawnPoint;
    public int nextSceneIndex;

    [Header("HUD")]
    public Image[] LiveSprites;
    public TextMeshProUGUI score;
    public TextMeshProUGUI parts;
    public TextMeshProUGUI fuelText;
    public TextMeshProUGUI getOuttaHereText;
    public float lerp = 10f;
    public float scoreLerpDuration = 2f;

    [Header("Game Tracking")]
    public int currentParts;
    public float newScore;
    public float scoreFloat;

    private IEnumerator coroutine;

    //public float fuel;
    //public float newFuel;

    // Start is called before the first frame update
    void Start()
    {
        scoreFloat = GameManager.instance.score;
    }

    // Update is called once per frame
    void Update()
    {

        ManagePlayerDeath();
        ManageLives();
        TrackScore();
        TrackWinCondition();
        //TrackFuel();
    }

    //This updates the UI for Score.
    public void TrackScore()
    {
        
        if(score != null)
        {
            score.text = scoreFloat.ToString("F0");

            if (newScore > scoreFloat)
            {
                lerp += Time.deltaTime / scoreLerpDuration;
                scoreFloat = (float)Mathf.Lerp(scoreFloat, newScore, lerp);
                score.text = scoreFloat.ToString("F0");
            }

            GameManager.instance.score = scoreFloat;
        }                   
    }

    //This updates the UI for parts.
    public void TrackWinCondition()
    {
        if (parts != null) 
        { 
            parts.text = currentParts.ToString() + "/" + partsNeeded.ToString();

            if (currentParts == partsNeeded)
            {
                getOuttaHereText.gameObject.SetActive(true);
                GameManager.instance.gotAllParts = true;

                GameObject player = GameObject.FindGameObjectWithTag("Player");
                player.GetComponent<ScreenWrap2>().enabled = false;
                Camera cam = Camera.main;

                bool CheckViewportPosition(Vector3 visTest)
                {
                    return ((visTest.x >= 0 && visTest.y >= 0) && (visTest.x <= 1 && visTest.y <= 1));
                }

                Vector3 viewportPosition = cam.WorldToViewportPoint(player.transform.position);
                bool isVisible = CheckViewportPosition(viewportPosition);

                if (!isVisible)
                {
                    SceneManager.LoadScene(nextSceneIndex);
                }
            
            }
        }
    }

    //public void TrackFuel()
    //{
        
    //    if (fuelText != null)
     //   {
     //       fuelText.text = "Fuel" + fuel.ToString("F0") + "%";
//
     //       if(newFuel != fuel)
   //         {
      //          lerp += Time.deltaTime / scoreLerpDuration;
      //          fuel = (float)Mathf.Lerp(fuel, newFuel, lerp);
     //           fuelText.text = "Fuel" + fuel.ToString("F0") + "%";
    //        }
   //     }
   // }

    //This increments the parts once the part is collected, it is triggered from the part itself on pickup.
    public void GetParts()
    {
            currentParts++;
    }

    //This increments the parts once the part is collected, it is triggered from the asteroid itself.
    public void GetScore(float scoreToAdd)
    {
        newScore += scoreToAdd;
    }

    public void ManagePlayerDeath()
    {
        if (GameManager.instance.Lives == 0)
        {
            coroutine = DeathWait(2.0f);
            StartCoroutine(coroutine);
            
        }

        if (GameManager.instance.isPlayerDead && GameManager.instance.Lives > 0)
        {
            coroutine = RespawnWait(2.0f);
            StartCoroutine(coroutine);
            
            GameManager.instance.isPlayerDead = false;

        }
    }

    private IEnumerator DeathWait(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        SceneManager.LoadScene(gameOverSceneIndex);

    }

    private IEnumerator RespawnWait(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        GameObject tempPlayer = Instantiate(GameManager.instance.Player, spawnPoint.transform.position, Quaternion.Euler(-90, 0, 0));
    }


    public void ManageLives()
    {
        if (GameManager.instance.Lives > 0)
        {
            LiveSprites[0].gameObject.SetActive(true);
        }

        else
        {
            LiveSprites[0].gameObject.SetActive(false);
        }

        if (GameManager.instance.Lives > 1)
        {
            LiveSprites[1].gameObject.SetActive(true);

        }

        else
        {
            LiveSprites[1].gameObject.SetActive(false);
        }

        if (GameManager.instance.Lives > 2)
        {
            LiveSprites[2].gameObject.SetActive(true);

        }

        else
        {
            LiveSprites[2].gameObject.SetActive(false);
        }

    }

    public void remoteGameOver()
    {
        SceneManager.LoadScene(gameOverSceneIndex);
    }
}
