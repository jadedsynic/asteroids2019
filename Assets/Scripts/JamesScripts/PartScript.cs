﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PartScript : MonoBehaviour
{
    public Rigidbody rb;
    private bool givenPart = false;
    private IEnumerator coroutine;

    // Start is called before the first frame update
    void Start()
    {
        coroutine = turnOnCollider(0.5f);
        StartCoroutine(coroutine);
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void ClampVelocity()
    {

    }

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {

            if (!givenPart)
            {
                GameManager.instance.LevelMaster.GetComponent<LevelMaster>().GetParts();
            }

            givenPart = true;

            this.gameObject.SetActive(false);
            Destroy(this.gameObject);
        }      

    }

    private IEnumerator turnOnCollider(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        GetComponent<BoxCollider>().enabled = true;
    }
}
