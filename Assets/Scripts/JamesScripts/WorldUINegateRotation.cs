﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldUINegateRotation : MonoBehaviour
{
    //This script is attached to a Canvas object in order to stop it rotating when it is the 
    //child of a gameobject that is rotating.

    //Keeps the ingame UI instance upright.
    private Quaternion rotation;
    private Vector3 position;

    void Awake()
    {
        rotation = transform.rotation;
        position = transform.parent.position - transform.position;
    }
    void LateUpdate()
    {
        transform.rotation = rotation;
        transform.position = transform.parent.position - position;
    }
}
