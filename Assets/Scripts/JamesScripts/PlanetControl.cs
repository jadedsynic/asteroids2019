﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum PlanetType
{
    partPlanet,
    fuelPlanet
    
}

public class PlanetControl : MonoBehaviour
{
    [Header("Planet Type")]
    public PlanetType planetType;

    //Orbiting Settings
    [Header("Orbiting settings")]
    public GameObject Sun;
    public Transform center;
    //Should be set to 0,0,1 in inspector
    public Vector3 axis = Vector3.forward;
    public float radius = 2.0f;
    public float radiusSpeed = 10f;
    //Change how fast the planet rotates around the center object
    public float rotationSpeed = 30f;

    [Header("Dispense Settings")]
    public BoxCollider dockingArea;
    public GameObject part;
    public GameObject powerUp;
    public Vector3 initialPickupForce;

    //Slider/Collision settings
    [Header("Slider settings")]
    public Slider attachedBar;
    public Image fillBarImage;
    public Color minBarColour;
    public Color maxBarColour;
    public float barSpeed = 10f;

    public float health = 100f;

    // Start is called before the first frame update
    void Start()
    {
        center = Sun.transform;
        transform.position = (transform.position - center.position).normalized * radius + center.position;

        attachedBar = GetComponentInChildren<Slider>();
        ResetSlider();

        initialPickupForce = new Vector3(Random.Range(-15f, 15f), Random.Range(-15f, 15f), 0);
    }


    // Update is called once per frame
    void Update()
    {
        transform.RotateAround(center.position, axis, rotationSpeed * Time.deltaTime);

        if (attachedBar != null)
        {
            attachedBar.value = Time.time;
            fillBarImage.color = Color.Lerp(minBarColour, maxBarColour, attachedBar.value / attachedBar.maxValue);

        }
        
    }

    public void ResetSlider()
    {
        if (attachedBar != null)
        {
            attachedBar.minValue = Time.time;
            attachedBar.maxValue = Time.time + barSpeed;
        }
    }

    public void Dispense()
    {
        

        switch (planetType)
        {
            case PlanetType.partPlanet:
                var DispensedPart = Instantiate(part, this.gameObject.transform.position, Quaternion.Euler(-90, 0, 0));
                DispensedPart.GetComponent<Rigidbody>().AddForce(initialPickupForce);
                var DispensedPowerUp = Instantiate(powerUp, this.gameObject.transform.position, Quaternion.identity);
                //DispensedPowerUp.GetComponent<Rigidbody>().AddForce(initialPickupForce);
                return;

            case PlanetType.fuelPlanet:

                return;
        }
       
    }

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.gameObject.tag == "Asteroid")
        {
            ResetSlider();
        }

    }

    //When an object enters the 'docking area' 
    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (attachedBar.value == attachedBar.maxValue)
            {
                ResetSlider();
                Dispense();
            }
        }
    }

    public void Damage(float damageAmount)

    {

        //subtract damage amount when Damage function is called

        health -= damageAmount;

        //Check if health has fallen below zero

        if (health <= 0)

        {
            gameObject.AddComponent<TriangleExplosion>();

            StartCoroutine(gameObject.GetComponent<TriangleExplosion>().SplitMesh(true));
            

        }
    }
}
