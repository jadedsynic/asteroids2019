﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour
{
    public ParticleSystem exPlosion;

    // Start is called before the first frame update
    private void Awake()
    {
        Destroy(gameObject, 5);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer == 11 || collision.gameObject.layer == 0 || collision.gameObject.layer == 12)
        {
            Instantiate(exPlosion, this.gameObject.transform.position, Quaternion.identity);
            Destroy(this.gameObject);
        }

    }

}
