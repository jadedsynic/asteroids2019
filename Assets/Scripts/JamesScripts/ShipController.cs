﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipController : MonoBehaviour
{
    public Rigidbody rb;


    [Header("Variables")]
    //Health
    public float health = 1;
    public float fuel = 100f;
    public float fuelUsageSpeed;

    //Movement Variables
    public float accelerationForce = 6f;
    public float rotationForce = 1f;
    public float maxSpeed = 5f;

    //Weapon Variables
    public float fireRate = 0.25f;
    private float nextFire;

    [Header("ParticleSystem")]
    public ParticleSystem ps;

    //[Header("ParicleOutPutSystem")]
    //public ParticleSystem OtP;


    [Header("Loadout")]
    public GameObject cannonlvl1;
    public GameObject cannonlvl2;
    public GameObject cannonlvl3;
    public GameObject wavelvl1;
    public GameObject wavelvl2;
    public GameObject wavelvl3;


    public ParticleSystem deathAnimation;

    private void Start()
    {
        //ignore the bullets layer to prevent collision physics occuring while firing
        Physics.IgnoreLayerCollision(9, 10);
    }

    void Update()
    {
        Movement();
        Shooting();

    }

    void Movement()
    {
        {
            //Movement, thrust
            float rotation = Input.GetAxis("Horizontal");
            float acceleration = Input.GetAxis("Vertical");
            rb.AddTorque(0, 0, -rotation * rotationForce);
            rb.AddForce(transform.forward * acceleration * accelerationForce);
            //limit the speed of the ship
            rb.velocity = Vector3.ClampMagnitude(rb.velocity, maxSpeed);
            //var emmission = OtP.emission;
            //emmission.rateOverTimeMultiplier = Mathf.Lerp(0f, 5f, rb.velocity.sqrMagnitude);
        }

    }

    void Shooting()
    {
        //Shooting
        if ((Input.GetButton("Fire1") || Input.GetKey(KeyCode.Space)) && Time.time > nextFire)
        {

            if (GameManager.instance.redPower == 1)
            {
                nextFire = Time.time + fireRate;
                Vector3 spawnLocation = gameObject.transform.TransformPoint(Vector3.forward * 0.5f);
                Quaternion spawnRotation = gameObject.transform.localRotation;
                var spawnedBullet = Instantiate(cannonlvl1, spawnLocation, Quaternion.Euler(0, 0, 0));
                Quaternion q = Quaternion.FromToRotation(Vector3.up, transform.forward);
                spawnedBullet.transform.rotation = q * spawnedBullet.transform.rotation;
                spawnedBullet.GetComponent<Rigidbody>().AddForce(transform.forward * 500);
                spawnedBullet.GetComponent<AudioSource>().Play();
            }

            if (GameManager.instance.redPower == 2)
            {
                nextFire = Time.time + fireRate;
                Vector3 spawnLocation = gameObject.transform.TransformPoint(Vector3.forward * 0.5f);
                Quaternion spawnRotation = gameObject.transform.localRotation;
                var spawnedBullet = Instantiate(cannonlvl2, spawnLocation, Quaternion.Euler(0, 0, 0));
                Quaternion q = Quaternion.FromToRotation(Vector3.up, transform.forward);

                for (int i = 0; i < spawnedBullet.transform.childCount; i++)
                {
                    var transform1 = spawnedBullet.transform.GetChild(i);
                    transform1.transform.rotation = q * spawnedBullet.transform.rotation;
                    //spawnedBullet.transform.rotation = q * spawnedBullet.transform.rotation;
                    transform1.GetComponent<Rigidbody>().AddForce(transform.forward * 500);
                    transform1.GetComponent<AudioSource>().Play();
                }


            }

            if (GameManager.instance.redPower == 3)
            {
                nextFire = Time.time + fireRate;
                Vector3 spawnLocation = gameObject.transform.TransformPoint(Vector3.forward * 0.5f);
                Quaternion spawnRotation = gameObject.transform.localRotation;
                var spawnedBullet = Instantiate(cannonlvl3, spawnLocation, Quaternion.Euler(0, 0, 0));
                Quaternion q = Quaternion.FromToRotation(Vector3.up, transform.forward);

                for (int i = 0; i < spawnedBullet.transform.childCount; i++)
                {
                    var transform1 = spawnedBullet.transform.GetChild(i);
                    transform1.transform.rotation = q * spawnedBullet.transform.rotation;
                    //spawnedBullet.transform.rotation = q * spawnedBullet.transform.rotation;
                    transform1.GetComponent<Rigidbody>().AddForce(transform.forward * 500);
                    transform1.GetComponent<AudioSource>().Play();
                }
            }

            if (GameManager.instance.greenPower == 1)
            {
                nextFire = Time.time + fireRate;
                Vector3 spawnLocation = gameObject.transform.TransformPoint(Vector3.forward * 0.5f);
                Quaternion spawnRotation = gameObject.transform.localRotation;
                var spawnedBullet = Instantiate(wavelvl1, spawnLocation, Quaternion.identity);
                Quaternion q = Quaternion.FromToRotation(Vector3.up, transform.forward);
                for (int i = 0; i < spawnedBullet.transform.childCount; i++)
                {
                    var transform1 = spawnedBullet.transform.GetChild(i);
                    transform1.transform.rotation = q * spawnedBullet.transform.rotation;
                    //spawnedBullet.transform.rotation = q * spawnedBullet.transform.rotation;
                    transform1.GetComponent<Rigidbody>().AddForce(transform.forward * 500);
                }

            }

            if (GameManager.instance.greenPower == 2)
            {
                nextFire = Time.time + fireRate;
                Vector3 spawnLocation = gameObject.transform.TransformPoint(Vector3.forward * 0.5f);
                Quaternion spawnRotation = gameObject.transform.localRotation;
                var spawnedBullet = Instantiate(wavelvl2, spawnLocation, Quaternion.Euler(0, 0, 0));
                Quaternion q = Quaternion.FromToRotation(Vector3.up, transform.forward);

                for (int i = 0; i < spawnedBullet.transform.childCount; i++)
                {
                    var transform1 = spawnedBullet.transform.GetChild(i);
                    transform1.transform.rotation = q * spawnedBullet.transform.rotation;
                    //spawnedBullet.transform.rotation = q * spawnedBullet.transform.rotation;
                    transform1.GetComponent<Rigidbody>().AddForce(transform.forward * 500);
                }


            }

            if (GameManager.instance.greenPower == 3)
            {
                nextFire = Time.time + fireRate;
                Vector3 spawnLocation = gameObject.transform.TransformPoint(Vector3.forward * 0.5f);
                Quaternion spawnRotation = gameObject.transform.localRotation;
                var spawnedBullet = Instantiate(wavelvl3, spawnLocation, Quaternion.Euler(0, 0, 0));
                Quaternion q = Quaternion.FromToRotation(Vector3.up, transform.forward);

                for (int i = 0; i < spawnedBullet.transform.childCount; i++)
                {
                    var transform1 = spawnedBullet.transform.GetChild(i);
                    transform1.transform.rotation = q * spawnedBullet.transform.rotation;
                    //spawnedBullet.transform.rotation = q * spawnedBullet.transform.rotation;
                    transform1.GetComponent<Rigidbody>().AddForce(transform.forward * 500);
                    transform1.GetComponent<AudioSource>().Play();
                }


            }
        }
    }

    public void Damage(float damageAmount)

    {

        //subtract damage amount when Damage function is called

        health -= damageAmount;

        //Check if health has fallen below zero

        if (health <= 0)

        {
            //if health has fallen below zero, lose a life and also put respawn here

            GameManager.instance.Lives--;
            GameManager.instance.isPlayerDead = true;
            Instantiate(ps, this.gameObject.transform.position, Quaternion.identity);
            this.gameObject.SetActive(false);
            Destroy(this.gameObject);
        }
    }

    //public void OutPutStream


}
