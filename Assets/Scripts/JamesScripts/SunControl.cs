﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SunControl : MonoBehaviour
{
    //Change these in the inspector to meddle with settings
    public float startDelay;
    public float timeIntervals;
    public Vector3 scaleIncreaseIncrements;
    public float growSpeed;

    void Start()
    {
        InvokeRepeating("GrowSun", startDelay, timeIntervals);
    }
    
    void GrowSun ()
    {
        transform.localScale = Vector3.Lerp(transform.localScale, scaleIncreaseIncrements, growSpeed * Time.deltaTime);
    }

    public void OnCollisionEnter(Collision collision)
    {
        
        if(collision.collider.gameObject.tag == "Player")
        {
            collision.collider.GetComponent<ShipController>().Damage(1000f);  
        }

        
            if(collision.collider.gameObject.layer == 11)
        {
            collision.collider.GetComponent<AsteroidScript>().Damage(1000f);
        }

        if (collision.collider.gameObject.layer == 12)
        {
            collision.collider.GetComponent<PlanetControl>().Damage(1000f);
        }

    }
}
