﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class HighScoreSceneScript : MonoBehaviour
{
    public Button mainMenuButton;
    public int mmSceneIndex;
    //public Text[] playerNamesText;
    //public Text[] scoresText;
    public TextMeshProUGUI[] playerNamesText;
    public TextMeshProUGUI[] scoresText;

    private PlayerNamesAndScores data = new PlayerNamesAndScores();

    // Start is called before the first frame update
    void Start()
    {
        mainMenuButton.onClick.AddListener(BacktoMM);
        data = GameManager.instance.localHighScoreList;

        for (int i = 0; i < data.playerName.Length; i++)
        {
            playerNamesText[i].text = data.playerName[i].ToString();

        }
        for (int i = 0; i < data.storedScores.Length; i++)
        {
            scoresText[i].text = data.storedScores[i].ToString();
        }

    }

    void BacktoMM()
    {
        SceneManager.LoadScene(mmSceneIndex);
    }
}
