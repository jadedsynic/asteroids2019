﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PowerUpType
{
    red,
    //green
}

public class PowerUpScript : MonoBehaviour
{
    public Rigidbody rb;

    public PowerUpType powerUp;
    public Animator thisAnimator;
    public Renderer rend;
    public int State;
    public Color redMat = Color.red;
    public Color greenMat = Color.green;
    public bool powerUpgiven = false;

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("ChangeColour", 2f, 4f);
        thisAnimator = GetComponent<Animator>();
        rend = GetComponent<Renderer>();
    }


    public void ChangeColour()
    {
        var numberOfPowerUpTypes = System.Enum.GetValues(typeof(PowerUpType)).Length;
        int pu = (int)powerUp;
        pu++;
        if (pu == numberOfPowerUpTypes) pu = 0;
        powerUp = (PowerUpType)pu;
    }

    // Update is called once per frame
    void Update()
    {
        switch (powerUp)
        {
            case PowerUpType.red:
                //thisAnimator.Play("PowerUpRed");
                rend.material.color = redMat;
                State = 1;
                break;

            //case PowerUpType.green:
                //thisAnimator.Play("PowerUpGreen");
             //   rend.material.color = greenMat;
             //   State = 3;
              //  break;
        }
    }

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {

            if (State == 1 && !powerUpgiven)
            {
                if (GameManager.instance.redPower == 0)
                {
                    GameManager.instance.redPower = 1;
                    GameManager.instance.greenPower = 0;

                    powerUpgiven = true;
                }

            }

            if (State == 1 && !powerUpgiven)
            {
                if (GameManager.instance.redPower == 1)
                {
                    GameManager.instance.redPower = 2;
                    GameManager.instance.greenPower = 0;

                    powerUpgiven = true;
                }
            }

            if (State == 1 && !powerUpgiven)
            {
                if (GameManager.instance.redPower == 2)
                {
                    GameManager.instance.redPower = 3;
                    GameManager.instance.greenPower = 0;

                    powerUpgiven = true;
                }

            }

            if (powerUpgiven)
            {
                this.gameObject.SetActive(false);
                Destroy(this.gameObject);
            }
        }

    }
}
