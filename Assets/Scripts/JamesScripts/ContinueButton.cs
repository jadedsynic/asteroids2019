﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ContinueButton : MonoBehaviour
{
    public Button continueButton;
    public int nextScreenIndex;

    // Start is called before the first frame update
    void Start()
    {
        continueButton.onClick.AddListener(Continue);
    }

void Continue()
    {
        SceneManager.LoadScene(nextScreenIndex);
    }
}
