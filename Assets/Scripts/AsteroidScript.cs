﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidScript : MonoBehaviour
{
    [Header("Asteroid Settings")]
    public float scoreAmount;
    public float health;
    public GameObject[] asteroidsToSplitInto;
    public Vector3 asteroidSplitForce;

    private IEnumerator coroutine;

    //Used to limit asteroid giving score on destruction more than once 
    private bool givenScore = false;


    // Start is called before the first frame update
    void Start()
    {
        coroutine = turnOnScreenWrap(2.0f);
        StartCoroutine(coroutine);
        Destroy(gameObject, 45);

        asteroidSplitForce = new Vector3(Random.Range(15f, 80f), Random.Range(15f, 80f), 0);
        Rigidbody rb = this.gameObject.GetComponent<Rigidbody>();
        rb.centerOfMass = Vector3.zero;
        rb.inertiaTensorRotation = Quaternion.identity;
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer == 10)
        {
            if (!givenScore)
            {
                GameManager.instance.LevelMaster.GetComponent<LevelMaster>().GetScore(scoreAmount);
                givenScore = true;
            }

            Damage(10f);
        }
        if (collision.gameObject.tag == "Player")
        {
            
            collision.collider.GetComponent<ShipController>().Damage(1f);
            Damage(10f);
        }
    }
    

    public void Damage(float damageAmount)

    {

        //subtract damage amount when Damage function is called

        health -= damageAmount;

        //Check if health has fallen below zero

        if (health <= 0)

        {
            for (int i = 0; i < asteroidsToSplitInto.Length; i++)
            {
                var splitAsteroid = Instantiate(asteroidsToSplitInto[i], this.transform.position, Quaternion.Euler(-90, 0, 0));
                splitAsteroid.GetComponent<Rigidbody>().AddForce(asteroidSplitForce);
            }
            //if health has fallen below zero, lose a life and also put respawn here
            this.gameObject.SetActive(false);
            Destroy(this.gameObject);
        }
    }

    private IEnumerator turnOnScreenWrap(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        GetComponent<ScreenWrap2>().enabled = true;
    }

}
