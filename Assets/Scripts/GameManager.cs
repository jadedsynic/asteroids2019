﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    //There should be a game manager placed in each scene. the Game Manager ensures that there is only one instance of the game running
    //And tracks scores between scenes indirectly 
    //it can also keep track of whether a controller is connected

    public static GameManager instance;

    //A copy of our player in case we need it
    public GameObject Player;
    public GameObject LevelMaster;

    [Header("Game Options")]
    public float GammaCorrection;

    //Tells levelmaster to respawn player if needed
    [Header("Game Flags")]
    public bool gotAllParts = false;
    public bool isPlayerDead = false;

    [Header("Game Tracking")]
    //Tracks lives between levels
    public float Lives = 3;
    public float score;

    [Header("Difficulty")]
    public int asteroidSpawnMultiplier;

    [Header("PowerUpLevels")]
    public int redPower = 1;
    public int greenPower;


    //Saving
    public delegate void SaveDelegate(object sender, EventArgs args);
    public static event SaveDelegate SaveEvent;
    public PlayerNamesAndScores localHighScoreList = new PlayerNamesAndScores();
    public PlayerNamesAndScores savedHighScoreList;




    void Awake()
    {

        if (instance == null)
        {
            DontDestroyOnLoad(gameObject);
            instance = this;
        }

        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        LoadData();
    }

    public void ResetLevel()
    {
        int y = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(y);
    }
    //Used to call on scene loaded
    void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    // this is used to minimise find game object with tag calls, it only happens once at the start of each level
    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        LevelMaster = GameObject.FindGameObjectWithTag("LevelMaster");
        gotAllParts = false;
    }

    // called when the game is terminated
    void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

//Saving
public void SaveData()
{
    if (!Directory.Exists("SaveData"))
        Directory.CreateDirectory("Saves");

    BinaryFormatter formatter = new BinaryFormatter();
    FileStream saveFile = File.Create("Saves/highscores.binary");

    GameManager.instance.savedHighScoreList = GameManager.instance.localHighScoreList;
    formatter.Serialize(saveFile, GameManager.instance.savedHighScoreList);

    saveFile.Close();

}

public void LoadData()
    {
        BinaryFormatter formatter = new BinaryFormatter();
        FileStream saveFile = File.Open("Saves/highscores.binary", FileMode.Open);
        GameManager.instance.localHighScoreList = (PlayerNamesAndScores)formatter.Deserialize(saveFile);

        saveFile.Close();


    }
}
