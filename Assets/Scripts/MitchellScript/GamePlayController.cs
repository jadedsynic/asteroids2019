﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamePlayController : MonoBehaviour
{
    //asteroid
    public int maxAsteroids = 10;
    public int asteroids = 0;
    public int asteroidsOnStart = 3;
    public float asteroidSpeed;

    //spawntimers
    public float secondsBetweenSpawns;
    public float elapsedTime = 0.0f;

    //spawnpoints
    public Transform[][] spawnPoints = new Transform[4][];

    //gameobjects
    public GameObject asteroid;

    // Start is called before the first frame update
    void Start()
    {
        //get all spawn point transforms
        spawnPoints = getTransforms();

        //for each starting asteroid
        for (int i = 0; i < asteroidsOnStart; i++)
        {
            //create an asteroid
            SpawnAsteroid();
        }
    }

    // Update is called once per frame
    void Update()
    {
        //increase the elapsed time
        elapsedTime += Time.deltaTime;

        //check time 
        if(elapsedTime > secondsBetweenSpawns && maxAsteroids > asteroids)
        {
            //reset the progressed time
            elapsedTime = 0;

            //Spawn an asteroid
            SpawnAsteroid();
        }
    }

    void SpawnAsteroid()
    {
        //determine which location to spawn from
        int randomSpawnSide = Random.Range(0, spawnPoints.Length);
        int randomSpawn = Random.Range(0, spawnPoints[randomSpawnSide].Length);

        //determine which point to head towards
        int toSpawnSide = Random.Range(0, spawnPoints.Length);
        while(toSpawnSide == randomSpawnSide) //check spawn side is not on the same side
            toSpawnSide = Random.Range(0, spawnPoints.Length);
        int toSpawnPoint = Random.Range(0, spawnPoints[toSpawnSide].Length);
        
        //set spawn point and point to move towards
        Transform spawnPoint = spawnPoints[randomSpawnSide][randomSpawn];
        Vector3 toPoint = spawnPoints[toSpawnSide][toSpawnPoint].position;

        //set spawn location, rotation and direction of the spawned asteroid
        Vector3 spawnLocation = spawnPoint.transform.position;
        Quaternion spawnRotation = spawnPoint.transform.rotation;
        Vector3 dir = (toPoint - spawnLocation).normalized;
        Vector3 forceDirection = dir * asteroidSpeed;

        //spawn asteroid
        var spawnedAsteroid = Instantiate(asteroid, spawnLocation, spawnRotation);
        //get rigidbody of the spawned asteroid and add force
        Rigidbody rb = spawnedAsteroid.GetComponent<Rigidbody>();
        rb.AddForce(forceDirection);
        //add random rotation to the asteroids
        Vector3 rotation = new Vector3(Random.Range(15, 100), Random.Range(15, 100), Random.Range(15, 100));
        rb.AddTorque(rotation);

        //increment the number of asteroids
        asteroids++;
    }

    Transform[][] getTransforms()
    {
        Transform[][] transforms = new Transform[4][];

        //find all spawn locations designated as a top spawn
        GameObject[] topSpawns = GameObject.FindGameObjectsWithTag("SpawnTop");
        if(topSpawns != null)
            transforms[0] = getTransform(topSpawns);

        //find all spawn locations designated as a top spawn
        GameObject[] bottomSpawns = GameObject.FindGameObjectsWithTag("SpawnBottom");
        if (bottomSpawns != null)
            transforms[1] = getTransform(bottomSpawns);

        //find all spawn locations designated as a top spawn
        GameObject[] leftSpawns = GameObject.FindGameObjectsWithTag("SpawnLeft");
        if (leftSpawns != null)
            transforms[2] = getTransform(leftSpawns);

        //find all spawn locations designated as a top spawn
        GameObject[] rightSpawns = GameObject.FindGameObjectsWithTag("SpawnRight");
        if (bottomSpawns != null)
            transforms[3] = getTransform(rightSpawns);

        //return all transforms
        return transforms;
    }

    Transform[] getTransform(GameObject[] spawns)
    {
        Transform[] transforms = new Transform[spawns.Length];

        //for each gameobject in the array find it's transform
        for (int i = 0; i < spawns.Length; i++)
            transforms[i] = spawns[i].transform;

        //return all transforms
        return transforms;
    }
}
