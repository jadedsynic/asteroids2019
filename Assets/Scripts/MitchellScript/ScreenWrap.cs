﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenWrap : MonoBehaviour
{
    //game components
    private Renderer[] renderers;
    public new Camera camera;

    //position booleans
    private bool isWrappingX = false;
    private bool isWrappingY = false;

    // Start is called before the first frame update
    void Start()
    {
        renderers = GetComponents<Renderer>();
    }

    bool CheckRenderers()
    {
        //check each render is visible
        foreach (Renderer renderer in renderers)
        {
            //if visible return true
            if (renderer.isVisible)
            {
                return true;
            }
        }

        //otherwise return false
        return false;
    }

    void ScreenWrapPosition()
    {
        //check if visible
        bool isVisible = CheckRenderers();

        if (isVisible)
        {
            isWrappingX = false;
            isWrappingY = false;
            return;
        }

        if(isWrappingX && isWrappingY)
        {
            return;
        }

        Vector3 viewportPosition = camera.WorldToViewportPoint(transform.position);
        print(viewportPosition);
        Vector3 newPosition = transform.position;

        if (!isWrappingX && (viewportPosition.x > 1 || viewportPosition.x < 0))
        {
            newPosition.x = -newPosition.x;

            isWrappingX = true;
        }

        if (!isWrappingY && (viewportPosition.y > 1 || viewportPosition.y < 0))
        {
            newPosition.y = -newPosition.y;

            isWrappingY = true;
        }

        transform.position = newPosition;
    }

    // Update is called once per frame
    void Update()
    {
        ScreenWrapPosition();
    }
}
