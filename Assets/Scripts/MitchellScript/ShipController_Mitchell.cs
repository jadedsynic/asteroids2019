﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipControllerMitchell : MonoBehaviour
{
    public Rigidbody rb;
    public GameObject bullet;

    //Movement Variables
    public float accelerationForce = 6f;
    public float rotationForce = 3f;
    public float maxSpeed = 5f;
    public float maxRotation = 2f;

    //Weapon Variables
    public float fireRate = 0.25f;
    private float nextFire;


    void FixedUpdate()
    {
        Movement();
        Shooting();

    }

    void Movement()
    {
        //Movement, thrust
        float rotation = -Input.GetAxis("Horizontal");
        float acceleration = Input.GetAxis("Vertical");

        rb.AddTorque(0, 0, rotation * rotationForce);

        rb.AddForce(transform.forward * acceleration * accelerationForce);
        rb.velocity = Vector3.ClampMagnitude(rb.velocity, maxSpeed);
    }

    void Shooting()
    {
        //Shooting
        if ((Input.GetButton("Fire1") || Input.GetKey(KeyCode.Space)) && Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            var spawnedBullet = Instantiate(bullet, gameObject.transform);
            spawnedBullet.GetComponent<Rigidbody>().AddForce(transform.forward * 500);
        }
    }
}
