﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenWrap2 : MonoBehaviour
{
    
    public Camera cam;
    private bool isWrappingX = true;
    private bool isWrappingY = true;

    private void Awake()
    {
        //cam = GameObject.FindObjectOfType<Camera>();
        cam = Camera.main;
    }

    bool CheckViewportPosition(Vector3 visTest)
    {
        return ((visTest.x >= 0 && visTest.y >= 0) && (visTest.x <= 1 && visTest.y <= 1));
    }

    void ScreenWrap()
    {
        Vector3 viewportPosition = cam.WorldToViewportPoint(transform.position);

        //check if visible
        bool isVisible = CheckViewportPosition(viewportPosition);

        if (isVisible)
        {
            isWrappingX = false;
            isWrappingY = false;
            return;
        }

        if (isWrappingX && isWrappingY)
        {
            return;
        }
        
        Vector3 newPosition = transform.position;

        if (!isWrappingX && (viewportPosition.x > 1 || viewportPosition.x < 0))
        {
            newPosition.x = -newPosition.x;

            isWrappingX = true;
        }

        if (!isWrappingY && (viewportPosition.y > 1 || viewportPosition.y < 0))
        {
            newPosition.y = -newPosition.y;

            isWrappingY = true;
        }

        transform.position = newPosition;
    }

    // Update is called once per frame
    void Update()
    {
        ScreenWrap();
    }
}
