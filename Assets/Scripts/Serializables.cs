﻿using System.Collections;
using System.Collections.Generic;
using System;

using UnityEngine;


[Serializable]
public class StoredScore
{
    public List<float> score;
}

[Serializable]
public class PlayerNamesAndScores
{
    public string[] playerName = new string[] { "", "", "", "", "", "", ""};
    public float[] storedScores = new float[] { 0f, 0f, 0f, 0f, 0f, 0f, 0f};


}